﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon;
using OranUnityUtils;

public class DamaNetworkManager : PunBehaviour
{
	private PhotonView myPhotonView;

	public void Connect()
	{
		PhotonNetwork.ConnectUsingSettings("0.1");
	}

	public override void OnJoinedLobby()
	{
		Debug.Log("JoinRandom");
		PhotonNetwork.JoinRandomRoom();
	}


	public override void OnConnectedToMaster()
	{
		// when AutoJoinLobby is off, this method gets called when PUN finished the connection (instead of OnJoinedLobby())
		PhotonNetwork.JoinRandomRoom();
	}

	public void OnPhotonRandomJoinFailed()
	{
		PhotonNetwork.CreateRoom(null);
	}

	public override void OnJoinedRoom()
	{
		Debug.Log("OnJoinedRoom");
		Player player = Player.None;

		if(PhotonNetwork.playerList.Length > 2){
			Debug.LogError("Only 2 player can connect to a Checkers room");
			return;
		}

		if(PhotonNetwork.playerList.Length == 1){
			player = Player.White;
			print ("I'm player white");
		}

		if(PhotonNetwork.playerList.Length == 2){
			player = Player.Black;
			print ("I'm player black");
		}

		GameObject damaPlayerObj = new GameObject("Local Dama Player "+(int)player);
//		GameObject playerObj = PhotonNetwork.Instantiate("HumanPlayer", Vector3.zero, Quaternion.identity, 0);
//	s	playerObj.name += " "+(int) player;
		IDamaPlayer damaPlayerScript = damaPlayerObj.GetOrAddComponent<HumanPlayer>();
		damaPlayerScript.Init(player);	

		InterfaceHelper.FindObjects<IDamaBoard>()
			.ForEach( d =>
				d.GetAllPiecesOfPlayer(player)
				.ForEach(p => 
					p.GameObject.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player)) );

		if(PhotonNetwork.playerList.Length == 2){
			print ("1");
			this.ExecuteDelayed(()=>StartGame(), 2f);
		}
	}

	public override void OnPhotonPlayerConnected(PhotonPlayer player)
	{
		print ("Player connected. Number of players is now " + PhotonNetwork.playerList.Length );
//		this.ExecuteDelayed(()=>{
//			if(PhotonNetwork.playerList.Length == 2){
//				IDamaPlayer whitePlayer = InterfaceHelper.FindObjects<IDamaPlayer>().Find(p => p.Player == Player.White);
//				IDamaPlayer blackPlayer = InterfaceHelper.FindObjects<IDamaPlayer>().Find(p => p.Player == Player.Black);
//				NetworkedGameManager.Instance.StartGame(whitePlayer, blackPlayer);
//				print ("starting game");
//			}
//		}, 2f);
//		print ("2");
//
//		this.ExecuteDelayed(()=>StartGame(), 2f);
	}

	private void StartGame(){
		IDamaPlayer whitePlayer = InterfaceHelper.FindObjects<IDamaPlayer>().Find(p => p.Player == Player.White);
		IDamaPlayer blackPlayer = InterfaceHelper.FindObjects<IDamaPlayer>().Find(p => p.Player == Player.Black);
		NetworkedGameManager.Instance.StartGame(whitePlayer, blackPlayer);
		print ("starting game");
	}

}
