﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RespawnPiece : MonoBehaviour {

	public GameObject objToCopy;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.R)) {
			(Instantiate(objToCopy) as GameObject).SetActive(true);
		}
	}
}
