﻿// C#
// SplitMeshIntoTriangles.cs
using UnityEngine;
using System.Collections;

public class SplitMeshIntoTriangles : MonoBehaviour
{
	private IEnumerator SplitMesh_Coro ()
	{
		MeshFilter MF = GetComponent<MeshFilter>();
		MeshRenderer MR = GetComponent<MeshRenderer>();
		Mesh M = MF.mesh;
		Vector3[] verts = M.vertices;
		Vector3[] normals = M.normals;
		Vector2[] uvs = M.uv;
		for (int submesh = 0; submesh < M.subMeshCount; submesh++)
		{
			int[] indices = M.GetTriangles(submesh);
			for (int i = 0; i < indices.Length; i += 3)
			{
				Vector3[] newVerts = new Vector3[3];
				Vector3[] newNormals = new Vector3[3];
				Vector2[] newUvs = new Vector2[3];
				for (int n = 0; n < 3; n++)
				{
					int index = indices[i + n];
					newVerts[n] = verts[index];
					newUvs[n] = uvs[index];
					newNormals[n] = normals[index];
				}
				Mesh mesh = new Mesh();
				mesh.vertices = newVerts;
				mesh.normals = newNormals;
				mesh.uv = newUvs;

				mesh.triangles = new int[] { 0, 1, 2, 2, 1, 0 };

				GameObject GO = new GameObject("Triangle " + (i / 3));
				GO.transform.position = transform.position;
				GO.transform.rotation = transform.rotation;
				GO.transform.localScale = transform.lossyScale;
				GO.AddComponent<MeshRenderer>().material = MR.materials[submesh];
				GO.AddComponent<MeshFilter>().mesh = mesh;
				GO.AddComponent<BoxCollider>();
				GO.AddComponent<Rigidbody>().AddExplosionForce(120, transform.position, 30);

				Destroy(GO, 2 + Random.Range(0.0f, 3.0f));
			}
		}
		MR.enabled = false;

//		Time.timeScale = 0.2f;
		yield return new WaitForSeconds(0.8f);
//		Time.timeScale = 1.0f;
		Destroy(gameObject);
	}

	private IEnumerator SplitMesh_Coro2 ()
	{
		MeshFilter MF = GetComponent<MeshFilter>();
		MeshRenderer MR = GetComponent<MeshRenderer>();
		Mesh M = MF.mesh;
		Vector3[] verts = M.vertices;
		Vector3[] normals = M.normals;
		Vector2[] uvs = M.uv;
		for (int submesh = 0; submesh < M.subMeshCount; submesh++)
		{
			int[] indices = M.GetTriangles(submesh);
			Mesh bigMesh = new Mesh();
			for (int i = 0; i < indices.Length; i += 3)
			{
				Vector3[] newVerts = new Vector3[3];
				Vector3[] newNormals = new Vector3[3];
				Vector2[] newUvs = new Vector2[3];
				for (int n = 0; n < 3; n++)
				{
					int index = indices[i + n];
					newVerts[n] = verts[index];
					newUvs[n] = uvs[index];
					newNormals[n] = normals[index];
				}
				Mesh mesh = new Mesh();
				mesh.vertices = newVerts;
				mesh.normals = newNormals;
				mesh.uv = newUvs;

				mesh.triangles = new int[] { 0, 1, 2, 2, 1, 0 };

				CombineInstance[] ci = new CombineInstance[1];
				ci[0] = new CombineInstance();
				ci[0].mesh = mesh;
				ci[0].subMeshIndex = i/3;
				bigMesh.CombineMeshes(ci);
			}

			GameObject GO = new GameObject("Triangle " + (submesh));
			GO.transform.position = transform.position;
			GO.transform.rotation = transform.rotation;
			GO.transform.localScale = transform.lossyScale;
			GO.AddComponent<MeshRenderer>().material = MR.materials[submesh];
			GO.AddComponent<MeshFilter>().mesh = bigMesh;
			GO.AddComponent<BoxCollider>();
			GO.AddComponent<Rigidbody>().AddExplosionForce(120, transform.position, 30);

			Destroy(GO, 2 + Random.Range(0.0f, 3.0f));
		}
		MR.enabled = false;

		//		Time.timeScale = 0.2f;
		yield return new WaitForSeconds(0.8f);
		//		Time.timeScale = 1.0f;
		Destroy(gameObject);
	}

	private IEnumerator SplitMesh_Coro3 ()
	{
		MeshFilter MF = GetComponent<MeshFilter>();
		MeshRenderer MR = GetComponent<MeshRenderer>();
		Mesh M = MF.mesh;
		Vector3[] verts = M.vertices;
		Vector3[] normals = M.normals;
		Vector2[] uvs = M.uv;
		for (int submesh = 0; submesh < M.subMeshCount; submesh++)
		{
			int[] indices = M.GetTriangles(submesh);
			for (int i = 0; i < indices.Length; i += 9)
			{
				Vector3[] newVerts = new Vector3[3];
				Vector3[] newNormals = new Vector3[3];
				Vector2[] newUvs = new Vector2[3];
				for (int n = 0; n < 3; n++)
				{
					int index = indices[i + n];
					newVerts[n] = verts[index];
					newUvs[n] = uvs[index];
					newNormals[n] = normals[index];
				}
				Mesh mesh = new Mesh();
				mesh.vertices = newVerts;
				mesh.normals = newNormals;
				mesh.uv = newUvs;

				mesh.triangles = new int[] { 0, 1, 2, 2, 1, 0 };

				GameObject GO = new GameObject("Triangle " + (i / 3));
				GO.transform.position = transform.position;
				GO.transform.rotation = transform.rotation;
				GO.transform.localScale = transform.lossyScale * 1.5f;
				GO.AddComponent<MeshRenderer>().material = MR.materials[submesh];
				GO.AddComponent<MeshFilter>().mesh = mesh;
				GO.AddComponent<BoxCollider>();
				GO.AddComponent<Rigidbody>().AddExplosionForce(330, transform.position, 30);

				Destroy(GO, 2 + Random.Range(0.0f, 3.0f));
			}
		}
		MR.enabled = false;

		//		Time.timeScale = 0.2f;
		yield return new WaitForSeconds(0.8f);
		//		Time.timeScale = 1.0f;
		Destroy(gameObject);
	}


	public void SplitMesh(){
		StartCoroutine(SplitMesh_Coro3());	
	}
}