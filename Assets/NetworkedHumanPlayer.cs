﻿using UnityEngine;
using System.Collections;

public class NetworkedHumanPlayer : HumanPlayer {

	public Player me;

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
		if(stream.isWriting){
			stream.SendNext((byte)Player);
		} else {
			this.Player = (Player) stream.ReceiveNext();
			me = this.Player;
		}
	}
}
