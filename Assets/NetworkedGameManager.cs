﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon;
using ExitGames.Client.Photon;

public enum GameMode{
	PlayerVsPlayer, PlayerVsPC
}

public class NetworkedGameManager : UnityEngine.MonoBehaviour {

	public static NetworkedGameManager Instance{get;private set;}

	protected Player playerTurn;
	protected IDamaBoard damaBoard;
	protected Dictionary<Player, IDamaPlayer> damaPlayers;
	protected IDamaPlayer localPlayer;

	//	public IDamaPlayer LocalPlayer {get; private set;} 
	//	public IDamaPlayer OpponentPlayer {get;private set;}

	protected List<IDamaBoard> damaBoards;

	private PhotonView myPhotonView;

	void Awake(){
		if(Instance==null){
			Instance = this;
		}
		damaBoards = InterfaceHelper.FindObjects<IDamaBoard>();
		damaPlayers = new Dictionary<Player, IDamaPlayer>();
		myPhotonView = GetComponent<PhotonView>();
	}

	public void Start(){
		PhotonPeer.RegisterType(typeof(IMove), (byte) 'A', SerializeMove, DeserializeMove);
		PhotonPeer.RegisterType(typeof(Move), (byte) 'B', SerializeMove, DeserializeMove);
		PhotonPeer.RegisterType(typeof(QueenMove), (byte) 'C', SerializeMove, DeserializeMove);
	}

	public void StartGame(IDamaPlayer whitePlayer, IDamaPlayer blackPlayer){
//		myPhotonView.RPC("StartGameRPC", PhotonTargets.All, localPlayer, opponentPlayer);	

//		damaPlayers.Add(whitePlayer.Player, whitePlayer);
//		damaPlayers.Add(blackPlayer.Player, blackPlayer);

		localPlayer = InterfaceHelper.FindObject<IDamaPlayer>();

		print ("Starting Game");

		if(IsGameOver()==false){
			StartTurn(Player.White);
		} else {

		}
	}

	public void StartTurn(Player player){
		myPhotonView.RPC("StartTurnRPC", PhotonTargets.All, (byte) player);
	}

	public void ApplyMove(IMove moveCommand){
		myPhotonView.RPC("ApplyMoveRPC", PhotonTargets.All, moveCommand);
	}

//	[PunRPC]
//	private void StartGameRPC(IDamaPlayer whitePlayer, IDamaPlayer blackPlayer){
//		damaPlayers.Add(whitePlayer.Player, whitePlayer);
//		damaPlayers.Add(blackPlayer.Player, blackPlayer);
//
//		StartTurn(Player.White);
//	}

	[PunRPC]
	private void StartTurnRPC(byte byteCastedPlayer){
		Player player = (Player) byteCastedPlayer;
//		IDamaPlayer playerScript = damaPlayers[player];
//		playerScript.AskForMove();
//		print("It is now "+player.ToString()+"'s turn");

		if(PhotonNetwork.isMasterClient && player == Player.White){
			var damaPlayer = InterfaceHelper.FindObject<IDamaPlayer>();
			if(damaPlayer.Player != Player.White){
				Debug.LogError("Le cose non tornano");
			}
			damaPlayer.AskForMove();
		} else 
		if(!PhotonNetwork.isMasterClient && player == Player.Black){
			var damaPlayer = InterfaceHelper.FindObject<IDamaPlayer>();
			if(damaPlayer.Player != Player.Black){
				Debug.LogError("Le cose non tornano");
			}
			damaPlayer.AskForMove();
		}
	}


	[PunRPC]
	private void ApplyMoveRPC(IMove moveCommand){
		damaBoards.ForEach( x => moveCommand.ApplyMove(x) );
		playerTurn = (Player) (((int) playerTurn +1)%2);
	
		if(IsGameOver()==false){
			StartTurn(playerTurn);
		} else {
			GameOverScreen();
		}

//		if(PhotonNetwork.isMasterClient && playerTurn == Player.White){
//			var damaPlayer = InterfaceHelper.FindObject<IDamaPlayer>();
//			if(damaPlayer.Player != Player.White){
//				Debug.LogError("Le cose non tornano");
//			}
//			damaPlayer.AskForMove();
//		} else 
//		if(!PhotonNetwork.isMasterClient && playerTurn == Player.Black){
//			var damaPlayer = InterfaceHelper.FindObject<IDamaPlayer>();
//			if(damaPlayer.Player != Player.Black){
//				Debug.LogError("Le cose non tornano");
//			}
//			damaPlayer.AskForMove();
//		}
	}

	private bool IsGameOver(){
		if(damaBoards[0].GetAllPiecesOfPlayer(Player.White).Count == 0){
			//Black Player wins
			return true;
		}
		if(damaBoards[0].GetAllPiecesOfPlayer(Player.Black).Count == 0){
			//White player wins
			return true;
		}
		return false;
	}

	private void GameOverScreen(){
		Player playerWhoWon;
		if(damaBoard.GetAllPiecesOfPlayer(Player.White).Count == 0){
			playerWhoWon = Player.Black;
		} else 
		if(damaBoard.GetAllPiecesOfPlayer(Player.Black).Count == 0){
			playerWhoWon = Player.White;
		} else {
			Debug.LogError("Nobody won yet");
			return;
		}

		if(localPlayer.Player == playerWhoWon){
			ShowWinScreen();
		} else {
			ShowLooseScreen();
		}
	}

	private void ShowWinScreen(){
		throw new System.NotImplementedException();
	}

	private void ShowLooseScreen(){
		throw new System.NotImplementedException();
	}


	private static byte[] SerializeMove(object move){
		IMove castedMove = (IMove) move;
		int byteArrayLength = sizeof(int) + (sizeof(int) * 4) + sizeof(int);
		byte[] serializedMove = new byte[byteArrayLength];
		int index = 0;

		//First byte defines the type of the move
		if(castedMove is Move){
			Protocol.Serialize(0, serializedMove, ref index);
		} else
		if(castedMove is QueenMove){
			Protocol.Serialize(1, serializedMove, ref index);
		}
			

		Protocol.Serialize(castedMove.StartX, serializedMove, ref index);
		Protocol.Serialize(castedMove.EndX, serializedMove, ref index);
		Protocol.Serialize(castedMove.StartY, serializedMove, ref index);
		Protocol.Serialize(castedMove.EndY, serializedMove, ref index);
		Protocol.Serialize((int)((byte)castedMove.MovingPlayer), serializedMove, ref index);

		return serializedMove;
	}

	private static object DeserializeMove(byte[] serializedMove){
		IMove move = null;
		int startX, startY, endX, endY;
		Player movingPlayer;

		int index = 0;
		int temp;
		int moveTypeId;
		Protocol.Deserialize(out moveTypeId, serializedMove, ref index);
		Protocol.Deserialize(out startX, serializedMove, ref index);
		Protocol.Deserialize(out endX, serializedMove, ref index);
		Protocol.Deserialize(out startY, serializedMove, ref index);
		Protocol.Deserialize(out endY, serializedMove, ref index);
		Protocol.Deserialize(out temp, serializedMove, ref index);
		movingPlayer = (Player) ((byte)((int)temp));

		if(moveTypeId == 0){
			move = new Move(startX, startY, endX, endY, movingPlayer);
		} else 
		if(moveTypeId == 1){
			move = new QueenMove(startX, startY, endX, endY, movingPlayer);
		}
		else {
			Debug.LogError("Error On Move Deserialization");
			return null;
		}

		return move;
	}

}
