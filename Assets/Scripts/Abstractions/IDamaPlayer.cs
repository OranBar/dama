﻿public interface IDamaPlayer {

	Player Player{get;}

	void Init(Player player);

	void AskForMove();

}
