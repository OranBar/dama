﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IMoveStrategy{

	//bool IsValidMove(int startX, int startY, int endX, int endY);
	List<IMove> GetValidMoves(int startX, int startY);
	List<IMove> GetValidCaptureMoves(int startX, int startY);

	List<ITile> GetValidMoveTiles(int startX, int startY);
	List<ITile> GetValidCaptureMoveTiles(int startX, int startY);

}
