﻿using UnityEngine;
using System.Collections;
using System;

public interface ITile {

	int X {get;set;}
	int Y {get;set;}

	event Action<ITile> OnTileSelection;

	void Init(int x, int y, IDamaBoard damaBoard);

	IPiece GetPieceOnTile();

	void HighlightTile(bool highlight);
}