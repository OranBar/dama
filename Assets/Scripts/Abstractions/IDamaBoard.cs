﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IDamaBoard {

	IPiece GetPiece(int x, int y);
	ITile GetTile(int x, int y);

	bool IsValidPiecePosition(int x, int y);
	bool IsBusy(int x, int y);

//	bool MovePiece(Move move);
//	bool MovePiece(IPiece piece, ITile tile);
//	bool MovePiece(IPiece piece, int endX, int endY);
//
//	bool IsValidMove(IPiece piece, int endX, int endY);

	void ApplyMoveCommand(IMove move);

	List<IPiece> GetAllPiecesOfPlayer(IDamaPlayer player);
	List<IPiece> GetAllPiecesOfPlayer (Player player);

	GameObject GetTileGameObject(int x, int y);
	Vector3 GetTileWorldPosition(int x, int y);

	//List<ITile> GetMovableTile(int startTileX, int startTileY, PieceType pieceType);
}