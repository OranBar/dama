﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public interface IPiece {

	PieceType PieceType{ get;}
	Player Owner {get;}
	GameObject GameObject{get;}

	event Action<IPiece> OnPieceSelection;

	int X {get;set;}
	int Y {get;set;}

	void Init(int x, int y, PieceType piece, Player owner, IDamaBoard damaBoard);
	void KillPiece();
	void PromoteToQueen();

	List<IMove> GetCaptureMoves(int startX, int startY);
	List<IMove> GetValidMoves(int startX, int startY);

	List<ITile> GetValidMoveTiles(int startX, int startY);
	List<ITile> GetCaptureMoveTiles(int startX, int startY);



//	void CreateMove(int startX, int startY, int endX, int endY);
}