﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IMove{

	int StartX {get;}
	int StartY {get;}
	int EndX {get;}
	int EndY {get;}

	Player MovingPlayer {get;}

//	List<IMove> Moves{ get;set; }

	IPiece GetMovingPiece(IDamaBoard damaBoard);
	ITile GetDestinationTile(IDamaBoard damaBoard);
	bool IsCapturingMove(IDamaBoard board);
	IPiece GetCapturedPiece(IDamaBoard board);
	void ApplyMove(IDamaBoard damaBoard);

}