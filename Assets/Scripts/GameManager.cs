﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//public enum GameMode{
//	PlayerVsPlayer, PlayerVsPC
//}

public class GameManager : MonoBehaviour {

	public static GameManager Instance{get;private set;}

	protected Player playerTurn;
	protected IDamaBoard damaBoard;
	protected Dictionary<Player, IDamaPlayer> damaPlayers;

//	public IDamaPlayer LocalPlayer {get; private set;} 
//	public IDamaPlayer OpponentPlayer {get;private set;}

	protected List<IDamaBoard> damaBoards;


	void Awake(){
		if(Instance==null){
			Instance = this;
		}
		damaBoards = InterfaceHelper.FindObjects<IDamaBoard>();
		damaPlayers = new Dictionary<Player, IDamaPlayer>();
	}

	//FIXME: Pezza
	void Start(){
		GameObject damaPlayerObj1 = new GameObject("Local Dama Player 1");
		IDamaPlayer damaPlayerScript1 = damaPlayerObj1.AddComponent<HumanPlayer>();
		damaPlayerScript1.Init(Player.White);

		GameObject damaPlayerObj2 = new GameObject("Local Dama Player 2");
		IDamaPlayer damaPlayerScript2 = damaPlayerObj2.AddComponent<HumanPlayer>();
		damaPlayerScript1.Init(Player.Black);

		StartGame(damaPlayerScript1, damaPlayerScript2);
	}

//	public void SelectMode(GameMode mode, IDamaPlayer localPlayer){
//		this.LocalPlayer = localPlayer;
//
//		switch(mode){
//			case GameMode.PlayerVsPlayer:
//				StartCoroutine( WaitForOpponentPlayer() );
//			break;
//			case GameMode.PlayerVsPC:
//				IDamaPlayer pcPlayer = null; //TODO
//				StartGame(localPlayer, pcPlayer);
//			break;
//		}
//	}
//
//	public IEnumerator WaitForOpponentPlayer(){
//		while(true){
//			if(OpponentPlayer != null){
//				StartGame(LocalPlayer, OpponentPlayer);
//			}
//			yield return new WaitForSeconds(0.5f);
//		}
//	}

	private void StartGame(IDamaPlayer localPlayer, IDamaPlayer opponentPlayer){
//		LocalPlayer = localPlayer;
//		OpponentPlayer = opponentPlayer;

		damaPlayers.Add(localPlayer.Player, localPlayer);
		damaPlayers.Add(opponentPlayer.Player, opponentPlayer);

		StartTurn(Player.White);
	}

	private void StartTurn(Player player){
		IDamaPlayer playerScript = damaPlayers[player];
		playerScript.AskForMove();
		print("It is now "+player.ToString()+"'s turn");
	}

	public void ApplyMove(IMove moveCommand){
		damaBoards.ForEach( x => moveCommand.ApplyMove(x) );
		playerTurn = (Player) (((int) playerTurn +1)%2);
		StartTurn(playerTurn);
	}

//	//FIXME: Pezza
//	public IDamaPlayer GetLocalPlayer(){
//		return InterfaceHelper.FindObject<IDamaPlayer>();
//	}
//	//FIXME: Pezza
//	public IDamaPlayer GetOpponentPlayer(){
//		//TODO: implement me
//		return InterfaceHelper.FindObject<IDamaPlayer>();
//	}



}
