﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class QueenPieceMovingStrategy : IMoveStrategy {

	private IDamaBoard damaBoard;
//	private IPiece piece;
	private Player pieceOwner;

	public QueenPieceMovingStrategy (IDamaBoard damaBoard, IPiece piece){
		this.damaBoard = damaBoard;
//		this.piece = piece;
		this.pieceOwner = piece.Owner;
	}

//	private bool IsValidMove(IMove move){
//		var piece = move.GetMovingPiece(damaBoard);
//		var destTile = move.GetDestinationTile(damaBoard);
//		return IsValidMove(piece.X, piece.Y, destTile.X, destTile.Y);
//	}

	public List<IMove> GetValidMoves (int startX, int startY){
		return GetDiagonalMoves(startX, startY, new Vector2(1,1))
			.Concat(GetDiagonalMoves(startX, startY, new Vector2(-1,  1)))
			.Concat(GetDiagonalMoves(startX, startY, new Vector2(-1, -1)))
			.Concat(GetDiagonalMoves(startX, startY, new Vector2( 1, -1)))
			.ToList();
	}

	private List<IMove> GetDiagonalMoves(int startX, int startY, Vector2 direction){
		
		List<IMove> diagonalMoves = new List<IMove>();
		int i = 0;
		int endX, endY;
		while(i<8){
			i++;
			endX = startX + (int)(direction.x * i);
			endY = startY + (int)(direction.y * i);
//			Debug.Log("Mehere1");
			IMove move = new QueenMove(startX, startY, endX, endY, pieceOwner);
//			if(damaBoard.IsValidPiecePosition(endX, endY)==false || damaBoard.IsBusy(endX, endY)){
//				break;
//			} else {
//				diagonalMoves.Add(move);
//			}

			if(damaBoard.IsValidPiecePosition(endX, endY)==false){
				return diagonalMoves;
			} else
			if(damaBoard.IsBusy(endX, endY)){
				if(damaBoard.GetPiece(endX, endY).Owner != pieceOwner){
						endX = startX + (int)(direction.x * (i+1) );
						endY = startY + (int)(direction.y * (i+1) );
						if(damaBoard.IsValidPiecePosition(endX, endY) && damaBoard.IsBusy(endX, endY)==false){
							//						Debug.Log("Mehere2");
							IMove newMove = new QueenMove(startX, startY, endX, endY, pieceOwner);
							diagonalMoves.Add(newMove);
						}
				}
				return diagonalMoves;
			} else {
				diagonalMoves.Add(move);
			}
		}

//		endX = startX + (int)(direction.x * ++i);
//		endY = startY + (int)(direction.y * ++i);
//		if(damaBoard.IsValidPiecePosition(endX, endY) && damaBoard.IsBusy(endX, endY)==false){
//			IMove move = new QueenMove(startX, startY, endX, endY);
//			diagonalMoves.Add(move);
//		}

		return diagonalMoves;

	}

	public List<IMove> GetValidCaptureMoves (int startX, int startY){
//		Debug.Log(GetValidMoves(startX, startY).FindAll(x => x.IsCapturingMove(damaBoard)).ToList().Count);
		return GetValidMoves(startX, startY).FindAll(x => x.IsCapturingMove(damaBoard)).ToList();
	}

	public List<ITile> GetValidMoveTiles (int startX, int startY)
	{
//		GetValidMoves(startX, startY).ForEach((IMove x)=>Debug.Log(x.ToString()));
		return GetValidMoves(startX, startY).Select(x => x.GetDestinationTile(damaBoard)).ToList();
	}

	public List<ITile> GetValidCaptureMoveTiles (int startX, int startY)
	{
		return GetValidCaptureMoves(startX, startY).Select(x => x.GetDestinationTile(damaBoard)).ToList();
	}
}
