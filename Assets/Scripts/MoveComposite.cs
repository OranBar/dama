﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System;
using Battlehub.Dispatcher;


public class MoveComposite  {

//	public int StartX {get; private set;}
//	public int StartY {get; private set;}
//	public int EndX {get; private set;}
//	public int EndY {get; private set;}

	public List<IMove> Moves{ get; set; }

	public volatile bool goNext = false;

	public MoveComposite (){
		Moves = new List<IMove>();
	}

	public void ApplyMove (IDamaBoard damaBoard){
		GameManager.Instance.StartCoroutine(ApplyMove_Coro(damaBoard));
	}

	private IEnumerator ApplyMove_Coro(IDamaBoard damaBoard){
		foreach(IMove move in Moves){
			move.ApplyMove(damaBoard);
			yield return new WaitForSeconds(1f);
		}
	}


//	public void ApplyMove (IDamaBoard damaBoard){
//		int movesToGo = Moves.Count;
//		goNext = true;
//
//		Thread thread = new Thread(()=> ThreadedApplyMove(this));
//		Thread stopperThread = new Thread(()=> ThreadStopper(thread, 6000));
//
//		Debug.Log("thread Created");
//		thread.Start();
//		stopperThread.Start();
//		Debug.Log("thread Starting");
//		while(!thread.IsAlive);
//
//		while(movesToGo>0){
//			if(goNext){
//				goNext = false;
//				Debug.Log("Applying move");
//				Moves[Moves.Count - movesToGo].ApplyMove(damaBoard);
//			} else {
//				Thread.Sleep((int)(Time.deltaTime * 49.9f));
//			}
//		}
//
//		Debug.Log("Thread rejoined");
//
////		Moves.ForEach( x=> x.ApplyMove(damaBoard) );
//	}
//
//	private void ThreadedApplyMove(MoveComposite move){
//		int j=0;
//
//		for (int i = 0; i < move.Moves.Count; i++) {
//			Debug.LogFormat("Applying {0}th move", ++i);
//			goNext = true;
//			Thread.Sleep(1000);
//		}
////		for (int i = 0; i < 3; i++) {
////			Debug.Log("Thread message: "+i);
////			Thread.Sleep(1000);
////		}
////		Debug.Log("Thread is done");
//	}
//
//	private void ThreadStopper(Thread thread, int milliseconds){
//		Thread.Sleep(milliseconds);
//		if(thread.IsAlive || thread.IsBackground){
//			Debug.Log("Stopper had to intervene");
//			thread.Abort();
//		}
//		return;
//	}
//




	public IPiece GetMovingPiece (IDamaBoard damaBoard){
		throw new System.Exception();
	}

	public ITile GetDestinationTile (IDamaBoard damaBoard){
		throw new System.Exception();
	}

	public bool IsCapturingMove (IDamaBoard damaBoard){
		throw new System.Exception();
	}

	public IPiece GetCapturedPiece (IDamaBoard board){
		throw new System.Exception();
	}


}
