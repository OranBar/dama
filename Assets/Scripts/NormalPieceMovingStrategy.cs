﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class NormalPieceMovingStrategy : IMoveStrategy {

	private IDamaBoard damaBoard;
	private IPiece myPiece;

	public NormalPieceMovingStrategy (IDamaBoard damaBoard, IPiece myPiece){
		this.damaBoard = damaBoard;
		this.myPiece = myPiece;
	}
	
	public bool IsValidMove(IPiece piece, int endX, int endY){
		return IsValidMove(piece.X, piece.Y, endX, endY);
	}

	public bool IsValidMove(int startX, int startY, int endX, int endY){
		bool isValidNonCapturingMove = IsValidNonCapturingMove (startX, startY, endX, endY);
		bool isValidCapturingMove = IsValidCapturingMove (startX, startY, endX, endY);

		return isValidNonCapturingMove || isValidCapturingMove;
	}

	private bool IsValidNonCapturingMove (int startX, int startY, int endX, int endY){
		if(damaBoard.IsValidPiecePosition (endX, endY)==false){
			return false;
		}
		bool isDestinationTileBusy = damaBoard.IsBusy(endX, endY);
		bool movingToAdjacentTile = (Mathf.Abs (startX - endX) <= 1 && Mathf.Abs (startY - endY) <= 1);
		bool isValidNonCapturingMove = !isDestinationTileBusy && movingToAdjacentTile;
		return isValidNonCapturingMove;
	}

	private bool IsValidCapturingMove (int startX, int startY, int endX, int endY){
		if(damaBoard.IsValidPiecePosition (endX, endY)==false){
			return false;
		}
		bool isEndPositionBusy = damaBoard.IsBusy(endX, endY);
		bool isCapturingMove = (Mathf.Abs (startX - endX) == 2 && Mathf.Abs (startY - endY) == 2);
		bool isAdjacentTileBusy = damaBoard.IsBusy ((int)Mathf.Lerp (startX, endX, 0.5f), (int)Mathf.Lerp (startY, endY, 0.5f));
		bool isAdjacentTileFriendly = isAdjacentTileBusy && damaBoard.GetPiece((int)Mathf.Lerp (startX, endX, 0.5f), (int)Mathf.Lerp (startY, endY, 0.5f)).Owner == myPiece.Owner;
		bool isValidcaptureMove = !isEndPositionBusy && !isAdjacentTileFriendly && isCapturingMove && isAdjacentTileBusy;
		return isValidcaptureMove;
	}

	public List<IMove> GetValidMoves(int startX, int startY){
		return (myPiece.Owner == Player.White) ? GetValidMovesForWhitePiece(startX, startY) : GetValidMovesForBlackPiece(startX, startY);
	}

	private List<IMove> GetValidMovesForWhitePiece(int startX, int startY){
		List<IMove> moves = new List<IMove>();

		if(IsValidMove(startX, startY, startX+1, startY+1)){
			moves.Add(new Move(startX, startY, startX+1, startY+1, myPiece.Owner));
		} 
		if(IsValidMove(startX, startY, startX-1, startY+1)){
			moves.Add(new Move(startX, startY, startX-1, startY+1, myPiece.Owner));
		}
		if(IsValidMove(startX, startY, startX+2, startY+2)){
			moves.Add(new Move(startX, startY, startX+2, startY+2, myPiece.Owner));
		} 
		if(IsValidMove(startX, startY, startX-2, startY+2)){
			moves.Add(new Move(startX, startY, startX-2, startY+2, myPiece.Owner));
		}
		return moves;
	}

	private List<IMove> GetValidMovesForBlackPiece(int startX, int startY){
//		List<ITile> tiles = new List<ITile>();
		List<IMove> moves = new List<IMove>();

		if(IsValidMove(startX, startY, startX+1, startY-1)){
//			tiles.Add(damaBoard.GetTile(startX+1, startY-1));
			moves.Add(new Move(startX, startY, startX+1, startY-1, myPiece.Owner));
		} 
		if(IsValidMove(startX, startY, startX-1, startY-1)){
//			tiles.Add(damaBoard.GetTile(startX-1, startY-1));
			moves.Add(new Move(startX, startY, startX-1, startY-1, myPiece.Owner));
		} 
		if(IsValidMove(startX, startY, startX+2, startY-2)){
//			tiles.Add(damaBoard.GetTile(startX+2, startY-2));
			moves.Add(new Move(startX, startY, startX+2, startY-2, myPiece.Owner));
		} 
		if(IsValidMove(startX, startY, startX-2, startY-2)){
//			tiles.Add(damaBoard.GetTile(startX-2, startY-2));
			moves.Add(new Move(startX, startY, startX-2, startY-2, myPiece.Owner));
		}
		return moves;
	}

	public List<IMove> GetValidCaptureMoves (int startX, int startY){
		return GetValidMoves(startX, startY).FindAll(x => x.IsCapturingMove(damaBoard));
	}

	public List<ITile> GetValidMoveTiles (int startX, int startY){
		return GetValidMoves(startX, startY).Select(x => x.GetDestinationTile(damaBoard)).ToList();
	}

	public List<ITile> GetValidCaptureMoveTiles (int startX, int startY){
		return GetValidCaptureMoves(startX, startY).Select(x => x.GetDestinationTile(damaBoard)).ToList();
	}

}
