﻿using UnityEngine;
using System.Collections;

public class DamaBoardUI : DamaBoard {

	public override void MovePieceVisualEffect (IMove move){
		ITile destinationTile = move.GetDestinationTile(this);
		IPiece movingPiece = move.GetMovingPiece(this);

		Vector3 endPosition = GetTileWorldPosition(destinationTile);
		GameObject pieceObj = pieceObjs[movingPiece.X, movingPiece.Y];
		pieceObj.transform.position = endPosition;
	}
}
