﻿using UnityEngine;
using System.Collections;
using OranUnityUtils;

public class DamaBoardGeneratorUI : MonoBehaviour {

	[Header("EditorScript")]
	public GameObject tilePrefab;
	public GameObject piecePrefab;

	[Header("Tile Parameters")]
	public float xOffset=50f;
	public float yOffset=50f;

	private DamaBoardUI boardScript;

	[ContextMenu("Generate Board")]
	public void GenerateAll(){
		this.transform.localScale = Vector3.one;
		GenerateBoard();
		GeneratePieces();
		this.transform.localScale = new Vector3(0.005f, 0.005f, 0.005f);
	}

	public void Awake(){
		GenerateAll();
	}

	public void GenerateBoard(){
		boardScript = this.gameObject.GetOrAddComponent<DamaBoardUI>();
		GameObject board = new GameObject("Board Container");
		board.transform.parent = this.transform;
		board.transform.localScale = Vector3.one;

		for(int y = 0; y < 8; y++){
			for (int x = 0; x < 8; x++) {
				GameObject tileObj = GenerateTile(x,y);
				tileObj.transform.SetParent(board.transform, false);
//				tileObj.transform.parent = board.transform;
				tileObj.transform.localScale = Vector3.one;
				boardScript.tileObjs[x,y] = tileObj;
			}
		}
	}

	public void GeneratePieces(){
		//		DamaBoard unityBoardScript = this.gameObject.GetOrAddComponent<DamaBoard>();
		//		for(int y = 0; y < 8; y++){
		//			for (int x = 0; x < 8; x++) {
		//				GameObject pieceObj = GeneratePiece(x,y);
		//				pieceObj.transform.parent = this.transform;
		//				unityBoardScript.pieceObjs[x,y] = pieceObj.GetComponent<Piece>();
		//			}
		//		}
		GameObject pieces = new GameObject("Pieces Container");
		pieces.transform.parent = this.transform;
		pieces.transform.localScale = Vector3.one;

		InitWhitePieces(pieces.transform);
		InitBlackPieces(pieces.transform);
	}

	private void InitWhitePieces(Transform piecesParent){
		DamaBoard boardScript = this.gameObject.GetOrAddComponent<DamaBoardUI>();

		for(int y=0; y<3; y++){
			for(int x=0; x<8 ; x++){
				if(boardScript.IsValidPiecePosition(x,y)){
					GameObject pieceObj = GeneratePiece(x,y, PieceType.Normal, Player.White);
					pieceObj.transform.SetParent(piecesParent, false);
					pieceObj.transform.localScale = Vector3.one;

					boardScript.pieceObjs[x,y] = pieceObj;
				}
			}
		}
	}

	private void InitBlackPieces(Transform piecesParent){
		DamaBoard boardScript = this.gameObject.GetOrAddComponent<DamaBoardUI>();

		for(int y=5; y<8; y++){
			for(int x=0; x<8 ; x++){
				if(boardScript.IsValidPiecePosition(x,y)){
					GameObject pieceObj = GeneratePiece(x,y, PieceType.Normal, Player.Black);
					pieceObj.transform.SetParent(piecesParent, false);
					pieceObj.transform.localScale = Vector3.one;

					boardScript.pieceObjs[x,y] = pieceObj;
				}
			}
		}
	}

	[ContextMenu("Destroy Board")]
	public void Destroy(){
		DestroyBoard();
		DestroyPieces();
	}

	private void DestroyBoard(){
		DestroyImmediate(this.transform.Find("Board Container").gameObject);
	}

	private void DestroyPieces(){
		DestroyImmediate(this.transform.Find("Pieces Container").gameObject);
	}

	private GameObject GenerateTile(int x, int y){
		Vector2 position = Vector2.zero;
		position.x = xOffset * x;
		position.y = yOffset * y;

		GameObject newTile = Instantiate(tilePrefab) as GameObject;
//		newTile.transform.localScale = Vector3.one;
		newTile.GetComponent<RectTransform>().anchoredPosition = position;
		newTile.name = newTile.name.Replace("Clone", x+" ,"+y);

		newTile.GetComponent<ITile>().Init(x,y, boardScript);

		return newTile;
	}

	private GameObject GeneratePiece(int x, int y, PieceType pieceType, Player pieceOwner){
		Vector2 position = Vector2.zero;
		position.x = xOffset * x;
		position.y = yOffset * y;

		GameObject newPiece = Instantiate(piecePrefab) as GameObject;
//		newPiece.transform.localScale = Vector3.one;
		newPiece.GetComponent<RectTransform>().anchoredPosition = position;

		newPiece.name = newPiece.name.Replace("Clone", x+" ,"+y);

		newPiece.GetComponent<IPiece>().Init(x, y, pieceType, pieceOwner, boardScript);

		return newPiece;
	}
}
