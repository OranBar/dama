﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using OranUnityUtils;

public class PieceUI : Piece {

//	public override PieceType PieceType {get;private set;}
//	public new Player Owner {get;set;}
//
//	public override int X {get;set;}
//	public override int Y {get;set;}

//	public event System.Action<IPiece> OnPieceSelection;

//	private IMoveStrategy moveStrategy;

//	private IDamaBoard damaBoard;

	public override void Init(int x, int y, PieceType piece, Player owner, IDamaBoard damaBoard){
		this.X = x;
		this.Y = y;
		this.PieceType = piece;
		this.Owner = owner;
		this.damaBoard = damaBoard;
		GetComponent<Image>().color = LoadColor();	
		moveStrategy = new NormalPieceMovingStrategy(damaBoard, this);
	}

	private Color LoadColor(){
		return (Owner == Player.White) ? Color.gray :  Color.black;
	}

	public override void KillPiece (){
		this.X = -1;
		this.Y = -1;

		this.ExecuteDelayed(()=>{
			this.GetComponent<Image>().enabled = false;
		}, 0.25f);

		this.ExecuteDelayed(()=>Destroy(this.gameObject), 5f);
	}

//	public override List<IMove> GetValidMoves (int startX, int startY){
//		return base.GetValidMoves (startX, startY);
//	}

	public override void PromoteToQueen (){
		this.PieceType = PieceType.Queen;
		this.moveStrategy = new QueenPieceMovingStrategy(damaBoard, this);

		if(Resources.Load("QueenPiece") as Sprite == null){
			Debug.LogWarning("Mehere");
		}
		GetComponent<Image>().sprite = Resources.Load<Sprite>("QueenPiece") ;
	}


}
