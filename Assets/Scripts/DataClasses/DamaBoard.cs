﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using System.Linq;

public enum PieceType {
	/*None=-1,*/ Normal, Queen
}

public enum Player : byte {
	White, Black, None
}

public class DamaBoard : MonoBehaviour, IDamaBoard {

	public GameObject[,] pieceObjs = new GameObject[8,8];
	public GameObject[,] tileObjs = new GameObject[8,8];

	void Awake(){
	//	GrabPiecesReferences();
	//	GrabTilesReferences();
	}
//	#region WillGoAwaySoon
//	private void GrabTilesReferences(){
//		foreach(Transform tile in this.transform.FindChildRecursive("Board Container")){
//			ITile tileScript = tile.GetComponent<ITile>();
//			tileObjs[tileScript.X, tileScript.Y] = tile.gameObject;
//		}
//	}
//
//	private void GrabPiecesReferences(){
//		foreach(Transform piece in this.transform.FindChildRecursive("Pieces Container")){
//			IPiece pieceScript = piece.GetComponent<IPiece>();
//			pieceObjs[pieceScript.X, pieceScript.Y] = piece.gameObject;
//		}
//	}
//	#endregion

	public IPiece GetPiece (int x, int y)
	{
		return pieceObjs[x,y].IfNotNull(z=>z.GetComponent<IPiece>());
	}

	public ITile GetTile (int x, int y)
	{
		return tileObjs[x,y].IfNotNull(z=>z.GetComponent<ITile>());
	}

	public bool IsValidPiecePosition(int x, int y){
		if(IsInBoard(x,y) == false){
			return false;
		} else {
			return (x+y)%2==0;
		}
	}

//	public bool MovePiece (Move move){
//		IPiece movingPiece = move.GetMovingPiece(this);
//
//		movingPiece.Move(move.endX, move.endY);
//
//		return true;
//	}

//	#region Doesn't belong here
//
//	public bool MovePiece (IPiece piece, int endX, int endY){
//		if(IsValidMove(piece.X, piece.Y, endX, endY)){
//			Vector3 endPosition = tileObjs[endX, endY].transform.position;
//			endPosition.y += 1f;
//			pieceObjs[piece.X, piece.Y].transform.position = endPosition;
//			piece.X = endX;
//			piece.Y = endY;
//			return true;
//		}
//		return false;
//	}
//
//	public bool MovePiece (IPiece piece, ITile tile){
//		return MovePiece(piece, tile.X, tile.Y);
//	}
//
//
//
//	public bool IsValidMove(IPiece piece, int endX, int endY){
//		return IsValidMove(piece.X, piece.Y, endX, endY);
//	}
//
//	public bool IsValidMove(int startX, int startY, int endX, int endY){
//		bool isValidNonCapturingMove = IsValidNonCapturingMove (startX, startY, endX, endY);
//		bool isValidCapturingMove = IsValidCapturingMove (startX, startY, endX, endY);
//
//		return isValidNonCapturingMove || isValidCapturingMove;
//	}
//
//	private bool IsValidNonCapturingMove (int startX, int startY, int endX, int endY){
//		//ITile destinationTile = tileObjs [endX, endY].GetComponent<ITile> ();
//		bool isDestinationTileBusy = IsBusy(endX, endY);
//		bool isValidEndPiecePosition = IsValidPiecePosition (endX, endY);
//		bool movingToAdjacentTile = (Mathf.Abs (startX - endX) <= 1 && Mathf.Abs (startY - endY) <= 1);
//		bool isValidNonCapturingMove = !isDestinationTileBusy && isValidEndPiecePosition && movingToAdjacentTile;
//		return isValidNonCapturingMove;
//	}
//
//	private bool IsValidCapturingMove (int startX, int startY, int endX, int endY){
//		bool isCapturingMove = (Mathf.Abs (startX - endX) == 2 && Mathf.Abs (startY - endY) == 2);
//		ITile middlewayTile = tileObjs [(int)Mathf.Lerp (startX, endX, 0.5f), (int)Mathf.Lerp (startY, endY, 0.5f)].GetComponent<ITile>();
//		bool isAdjacentTileBusy = IsBusy (middlewayTile);
//		bool isValidcaptureMove = isCapturingMove && isAdjacentTileBusy;
//		return isValidcaptureMove;
//	}
//
//	public List<ITile> GetMovableTiles(int startX, int startY){
//		List<ITile> tiles = new List<ITile>();
//
//		if(IsValidMove(startX, startY, startX+1, startY+1)){
//			tiles.Add(tileObjs[startX+1, startY+1].GetComponent<ITile>());
//		} 
//		if(IsValidMove(startX, startY, startX-1, startY+1)){
//			tiles.Add(tileObjs[startX-1, startY+1].GetComponent<ITile>());
//		} 
//		if(IsValidMove(startX, startY, startX+2, startY+2)){
//			tiles.Add(tileObjs[startX+2, startY+2].GetComponent<ITile>());
//		} 
//		if(IsValidMove(startX, startY, startX-2, startY+2)){
//			tiles.Add(tileObjs[startX-2, startY+2].GetComponent<ITile>());
//		}
//
//		//		if(IsInBoard(newX, newY)){
//		//			currentPiece = pieceObjs[newX, newY].GetComponent<IPiece>();
//		//			if(currentPiece.PieceType == PieceType.None){
//		//				tiles.Add(tileObjs[newX, newY].GetComponent<ITile>());
//		//			} 
//		//			else if(IsInBoard(newX+i, newY+1)){
//		//				currentPiece = pieceObjs[newX, newY].GetComponent<IPiece>();
//		//				if(currentPiece.PieceType == PieceType.None){
//		//					tiles.Add(tileObjs[newX, newY].GetComponent<ITile>());
//		//				} 
//		//			}
//		//			else if(  ){
//		//				
//		//			}
//		//TODO
//		return tiles;
//	}
//
//	public List<ITile> GetMovableTiles(ITile startTile){
//		return GetMovableTiles(startTile.X, startTile.Y);
//	}
//
//	public List<ITile> GetMovableTiles(IPiece startPiece){
//		return startPiece.GetValidMoveTiles(startPiece.X, startPiece.Y);
//	}
//
//	#endregion


	public bool IsBusy(int x, int y){
		return GetPiece(x,y) != null;
	}

	private bool IsBusy(ITile tile){
		return tile.GetPieceOnTile() != null;
	}

	private bool IsInBoard(int x, int y){
		return x>=0 && x<8 && y>=0 && y<8;
	}

//	public void ApplyMoveCommand (Move move){
//		IPiece movingPiece = move.GetMovingPiece(this);
//
//		movingPiece.Move(move.endX, move.endY);
//
//		pieceObjs[move.endX, move.endY] = pieceObjs[move.startX, move.startY];
//		pieceObjs[move.startX, move.startY] = null;
//		move.GetCapturedPiece(this).IfNotNull(p => pieceObjs[p.X, p.Y] = null);
//
//	}

	public virtual void ApplyMoveCommand (IMove move){
		IPiece movingPiece = move.GetMovingPiece(this);
		ITile destinationTile = move.GetDestinationTile(this);
//
//		Vector3 newPosition = GetTileWorldPosition(destinationTile);
//		newPosition.y += 1f;
		MovePieceVisualEffect(move);

		pieceObjs[destinationTile.X, destinationTile.Y] = pieceObjs[movingPiece.X, movingPiece.Y];
		pieceObjs[movingPiece.X, movingPiece.Y] = null;

		movingPiece.X = destinationTile.X;
		movingPiece.Y = destinationTile.Y;

		if(move.IsCapturingMove(this)){	
			IPiece capturedPiece = move.GetCapturedPiece(this);
			pieceObjs[capturedPiece.X, capturedPiece.Y] = null;
			capturedPiece.KillPiece();
		}

		if(move.GetDestinationTile(this).Y == 0 && move.GetMovingPiece(this).Owner == Player.Black ){
			move.GetMovingPiece(this).PromoteToQueen();
		}
		if(move.GetDestinationTile(this).Y == 7 && move.GetMovingPiece(this).Owner == Player.White ){
			move.GetMovingPiece(this).PromoteToQueen();
		}
	}

	public virtual void MovePieceVisualEffect(IMove move){
		IPiece movingPiece = move.GetMovingPiece(this);
		ITile destinationTile = move.GetDestinationTile(this);

		Vector3 newPosition = GetTileWorldPosition(destinationTile);
		newPosition.y += 1f;
		pieceObjs[movingPiece.X, movingPiece.Y].transform.position = newPosition;
	}

	public List<IPiece> GetAllPiecesOfPlayer (Player player){
		List<IPiece> result = new List<IPiece>();
		for(int y=0; y<8; y++){
			for (int x = 0; x < 8; x++) {
				if(IsValidPiecePosition(x,y)){
					GameObject pieceObj = pieceObjs[x,y];
					if(pieceObj!=null){
						IPiece piece = pieceObj.GetComponent<IPiece>();
						if(piece.Owner == player){
							result.Add(piece);
						}

					}
				}
			}
		}
		return result;
	}

	public List<IPiece> GetAllPiecesOfPlayer (IDamaPlayer player){
		List<IPiece> result = new List<IPiece>();
		for(int y=0; y<8; y++){
			for (int x = 0; x < 8; x++) {
				if(IsValidPiecePosition(x,y)){
					GameObject pieceObj = pieceObjs[x,y];
					if(pieceObj!=null){
						IPiece piece = pieceObj.GetComponent<IPiece>();
						if(piece.Owner == player.Player){
							result.Add(piece);
						}

					}
				}
			}
		}
		return result;
	}

	public GameObject GetTileGameObject (int x, int y){
		return tileObjs[x,y];
	}

	public Vector3 GetTileWorldPosition(int x, int y){
		return tileObjs[x,y].transform.position;
	}

	public Vector3 GetTileWorldPosition(ITile tile){
		return tileObjs[tile.X, tile.Y].transform.position;
	}
		
}
