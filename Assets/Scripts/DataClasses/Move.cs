﻿using UnityEngine;
using System.Collections.Generic;

public class Move : IMove {

	public int StartX {get; private set;}
	public int StartY {get; private set;}
	public int EndX {get; private set;}
	public int EndY {get; private set;}

	public Player MovingPlayer{get;private set;}

//	public List<IMove> Moves{ get; set; }

	public Move(){
		
	}

	public Move (int startX, int startY, int endX, int endY, Player pieceOwner){
		this.StartX = startX;
		this.StartY = startY;
		this.EndX = endX;
		this.EndY = endY;
		this.MovingPlayer = pieceOwner;
//		Moves = new List<IMove>();
	}

	public Move(IPiece piece, int EndX, int EndY, Player pieceOwner) : this (piece.X, piece.Y, EndX, EndY, pieceOwner){

	}

	public Move(IPiece piece, ITile targetTile, Player pieceOwner) : this (piece.X, piece.Y, targetTile.X, targetTile.Y, pieceOwner){

	}

	public IPiece GetMovingPiece(IDamaBoard damaBoard){
		return damaBoard.GetPiece(StartX, StartY).Return(x=>x, damaBoard.GetPiece(EndX, EndY));
	}

	public ITile GetDestinationTile(IDamaBoard damaBoard){
		return damaBoard.GetTile(EndX, EndY);
	}

	public bool IsCapturingMove(IDamaBoard damaBoard){
		return Mathf.Abs(StartX - EndX) == 2;
	}

	public IPiece GetCapturedPiece(IDamaBoard damaBoard){
		if(IsCapturingMove(damaBoard)==false){
			return null;
		} else {
			return damaBoard.GetPiece((int)Mathf.Lerp(StartX, EndX, 0.5f), (int)Mathf.Lerp(StartY, EndY, 0.5f));
		}
	}

	public void ApplyMove (IDamaBoard damaBoard){
		damaBoard.ApplyMoveCommand(this);
	}

	public override string ToString ()
	{
		return string.Format ("[Move: StartX={0}, StartY={1}, EndX={2}, EndY={3}]", StartX, StartY, EndX, EndY);
	}
}