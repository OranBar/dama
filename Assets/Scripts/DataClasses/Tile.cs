﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Tile : MonoBehaviour, ITile, IPointerClickHandler {

	//public GameObject highlightTilePrefab;

	public int X {get;set;}
	public int Y {get;set;}

	public event System.Action<ITile> OnTileSelection;

	private IDamaBoard damaBoard;
	private GameObject highlightObject;

	public virtual void Init (int x, int y){
		this.X = x;
		this.Y = y;
		GetComponent<Renderer>().material = LoadMaterial(x, y);	
	}

	public virtual void Init(int x, int y, IDamaBoard damaBoard){
		Init(x,y);
		this.damaBoard = damaBoard;

		highlightObject = this.transform.GetChild(0).gameObject;
	}

	private Material LoadMaterial(int x, int y){
		string myMaterialname = ((x+y)%2==1) ? "WhiteTileW" :  "BlackTileW";
		return Resources.Load(myMaterialname) as Material;
	}

	public void OnPointerClick(PointerEventData data){
	//	player.MoveSelectedPiece(this);
		OnTileSelection.Do( x => x(this));
	}

	public IPiece GetPieceOnTile(){
		return damaBoard.GetPiece(X, Y);
	}

	public IPiece GetPieceOnTile(IDamaBoard damaBoard){
		return damaBoard.GetPiece(X, Y);
	}

	public virtual void HighlightTile (bool highlight){
		highlightObject.SetActive(highlight);
	}

}
