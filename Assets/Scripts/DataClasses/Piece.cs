﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;
using OranUnityUtils;

public class Piece : MonoBehaviour, IPiece, IPointerClickHandler {

	public GameObject queenContainerGo;
	public ParticleSystem powerUpParticle;

	public virtual PieceType PieceType {get;protected set;}
	public Player Owner {get;protected set;}
	public GameObject GameObject {
		get { return gameObject; }
	}


	public virtual int X {get;set;}
	public virtual int Y {get;set;}

	protected IMoveStrategy moveStrategy;

	private HumanPlayer player;
	protected IDamaBoard damaBoard;

	public event Action<IPiece> OnPieceSelection;


	//	public void Init (int x, int y, PieceType piece, Player owner){
//		this.X = x;
//		this.Y = y;
//		this.PieceType = piece;
//		this.Owner = owner;
//		GetComponent<Renderer>().material = LoadMaterial();	
//	}

	public virtual void Init(int x, int y, PieceType piece, Player owner, IDamaBoard damaBoard){
		this.X = x;
		this.Y = y;
//		this.PieceType = piece;
		this.PieceType = PieceType.Normal; 
		this.Owner = owner;
		GetComponent<Renderer>().material = LoadMaterial();	
		this.damaBoard = damaBoard;
		moveStrategy = new NormalPieceMovingStrategy(damaBoard, this);
	}

	private Material LoadMaterial(){
		string myMaterialname = (Owner == Player.White) ? "WhitePiece" :  "BlackPiece";
		return Resources.Load(myMaterialname) as Material;
	}

	public void OnPointerClick(PointerEventData data){
//		if(Owner == GameManager.Instance.GetLocalPlayer().Player){
//			player.SelectedPiece = this;
//			print(player.SelectedPiece);
//		}
		print (gameObject.name+" was clicked");

		OnPieceSelection.Do( x => x(this) );
	}

	public override string ToString (){
		return string.Format ("[Piece: X={0}, Y={1}]", X, Y);
	}

//	public List<ITile> GetMovableTiles(int startX, int startY){
//		return moveStrategy.GetValidMoves(startX, startY);
//	}

	public virtual void KillPiece (){
		GameObject explosionObj = this.transform.GetChild(0).gameObject;
		explosionObj.SetActive(true);

		this.X = -1;
		this.Y = -1;

		this.ExecuteDelayed(()=>GetComponent<SplitMeshIntoTriangles>().SplitMesh(), 0.1f);

		this.ExecuteDelayed(()=>{
//			this.GetComponent<MeshRenderer>().enabled = false;
			Destroy(this.GetComponent<Rigidbody>());
			this.GetComponent<Collider>().enabled = false;
		}, 0.25f);


		this.ExecuteDelayed(()=>Destroy(this.gameObject), 5f);
	}

	public virtual List<IMove> GetCaptureMoves (int startX, int startY){
		return moveStrategy.GetValidCaptureMoves(startX, startY);
	}

	public virtual List<IMove> GetValidMoves (int startX, int startY){
		return moveStrategy.GetValidMoves(startX, startY);
	}

	public virtual List<ITile> GetValidMoveTiles (int startX, int startY){
		return moveStrategy.GetValidMoveTiles(startX, startY);
	}

	public virtual List<ITile> GetCaptureMoveTiles (int startX, int startY){
		return moveStrategy.GetValidCaptureMoveTiles(startX, startY);
	}

	public virtual void PromoteToQueen(){
		this.PieceType = PieceType.Queen;
		this.moveStrategy = new QueenPieceMovingStrategy(damaBoard, this);

		powerUpParticle.Play();
		this.ExecuteDelayed(()=>queenContainerGo.SetActive(true), 1f);
	}
}
