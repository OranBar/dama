﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TileUI : Tile {

//	public int X {get;set;}
//	public int Y {get;set;}

//	public event System.Action<ITile> OnTileSelection;

//	private IDamaBoard damaBoard;

	public override void Init(int x, int y, IDamaBoard damaBoard){
		this.X = x;
		this.Y = y;
//		this.damaBoard = damaBoard;
		GetComponent<Image>().color = LoadColor(x, y);	
	}

	private Color LoadColor(int x, int y){
		return (((x+y)%2!=0)) ? Color.white :  new Color(0.5f,0f,0f,1f);
	}

	public override void HighlightTile (bool highlight)
	{
		Color color = new Color(0.2f,0.2f,0f,1f);
		if(highlight){
			GetComponent<Image>().color -= color;
		} else {
			GetComponent<Image>().color += color;
		}
	}

}
