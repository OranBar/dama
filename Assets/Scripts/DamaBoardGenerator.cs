﻿using UnityEngine;
using System.Collections;
using OranUnityUtils;

public class DamaBoardGenerator : MonoBehaviour {

	[Header("EditorScript")]
	public GameObject tilePrefab;
	public GameObject piecePrefab;

	[Header("Tile Parameters")]
	public float xOffset=1f;
	public float zOffset=1f;

	[Header("Piece Parameter")]
	public float yOffset = 1f;

	private DamaBoard boardScript;

	[ContextMenu("Generate Board")]
	public void GenerateAll(){
		GenerateBoard();
		GeneratePieces();
	}

	public void Awake(){
		RegenerateBoard();
	}

	public void GenerateBoard(){
		boardScript = this.gameObject.GetOrAddComponent<DamaBoard>();
		GameObject board = new GameObject("Board Container");
		board.transform.parent = this.transform;

		for(int y = 0; y < 8; y++){
			for (int x = 0; x < 8; x++) {
				GameObject tileObj = GenerateTile(x,y);
				tileObj.transform.parent = board.transform;
				boardScript.tileObjs[x,y] = tileObj;
			}
		}
	}

	public void GeneratePieces(){
		GameObject pieces = new GameObject("Pieces Container");
		pieces.transform.parent = this.transform;

		InitWhitePieces(pieces.transform);
		InitBlackPieces(pieces.transform);
	}

	private void InitWhitePieces(Transform piecesParent){
		DamaBoard boardScript = this.gameObject.GetOrAddComponent<DamaBoard>();

		for(int y=0; y<3; y++){
			for(int x=0; x<8 ; x++){
				if(boardScript.IsValidPiecePosition(x,y)){
					GameObject pieceObj = GeneratePiece(x,y, PieceType.Normal, Player.White);
					pieceObj.transform.parent = piecesParent;

					boardScript.pieceObjs[x,y] = pieceObj;
				}
			}
		}
	}

	private void InitBlackPieces(Transform piecesParent){
		DamaBoard boardScript = this.gameObject.GetOrAddComponent<DamaBoard>();

		for(int y=5; y<8; y++){
			for(int x=0; x<8 ; x++){
				if(boardScript.IsValidPiecePosition(x,y)){
					GameObject pieceObj = GeneratePiece(x,y, PieceType.Normal, Player.Black);
					pieceObj.transform.parent = piecesParent;

					boardScript.pieceObjs[x,y] = pieceObj;
				}
			}
		}
	}

	[ContextMenu("Destroy Board")]
	public void Destroy(){
		DestroyBoard();
		DestroyPieces();
	}

	private void DestroyBoard(){
		DestroyImmediate(this.transform.Find("Board Container").gameObject);
	}

	private void DestroyPieces(){
		DestroyImmediate(this.transform.Find("Pieces Container").gameObject);
	}

	private GameObject GenerateTile(int x, int y){
		Vector3 position = Vector3.zero;
		position.x = xOffset * x;
		position.z = zOffset * y;

		GameObject newTile = Instantiate(tilePrefab, position, Quaternion.identity) as GameObject;
		newTile.name = newTile.name.Replace("Clone", x+" ,"+y);

		newTile.GetComponent<ITile>().Init(x,y, boardScript);

		return newTile;
	}

	private GameObject GeneratePiece(int x, int y, PieceType pieceType, Player pieceOwner){
		Vector3 position = Vector3.zero;
		position.x = xOffset * x;
		position.z = zOffset * y;
		position.y = yOffset;

		GameObject newPiece = Instantiate(piecePrefab, position, Quaternion.identity) as GameObject;
		newPiece.name = newPiece.name.Replace("Clone", x+" ,"+y);

		newPiece.GetComponent<IPiece>().Init(x, y, pieceType, pieceOwner, boardScript);

		return newPiece;
	}

	[ContextMenu("Regenerate Board")]
	public void RegenerateBoard(){
		DestroyBoard();
		DestroyPieces();
		GenerateBoard();
		GeneratePieces();
	}
}
