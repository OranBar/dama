﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum TurnState{
	OpponentTurn, SelectingPiece, SelectingDestinationTile
}

public class HumanPlayer : MonoBehaviour, IDamaPlayer {

	public Player Player{get;set;}
	public TurnState TurnState{get;set;}
	public IPiece SelectedPiece {get;set;}

	public List<IDamaBoard> damaBoards;

	private NetworkedGameManager gameManager;
	private IMove currentMove;


	void Awake(){
		TurnState = TurnState.OpponentTurn;
		damaBoards = InterfaceHelper.FindObjects<IDamaBoard>();
		gameManager = NetworkedGameManager.Instance;
		SelectedPiece = null;
	}

	public void Init (Player player){
		this.Player = player;
	}

	public void AskForMove (){
		TurnState = TurnState.SelectingPiece;

		RegisterSelectPieceEvent();
	}

	public void PieceSelected(IPiece piece){
		if(SelectedPiece != null){
			UnRegisterMovableTilesForSelectedTileEvent(SelectedPiece.X, SelectedPiece.Y);
		}

		SelectedPiece = piece;
		print("Selected Piece is " + SelectedPiece.X + " " + SelectedPiece.Y);

		RegisterMovableTilesForSelectedTileEvent(piece.X, piece.Y);
	}

	private void TileSelected(ITile tile){
//		IMove myMove;
		int startTileX, startTileY;
		UnRegisterSelectedPieceEvent();
		if(currentMove != null){
			//I don't think I ever get here
			Debug.LogError("I'm not supposed to be here");
			/*
			IMove lastMove = currentMove.Moves[currentMove.Moves.Count-1];
			ITile lastMoveDestination = lastMove.GetDestinationTile(damaBoards[0]);
			startTileX = lastMoveDestination.X;
			startTileY = lastMoveDestination.Y;
			*/
		} else {
//			currentMove = new MoveComposite();
			startTileX = SelectedPiece.X;
			startTileY = SelectedPiece.Y;
		} 
		//Moved down from the else, since I always hit the else (i think)
		startTileX = SelectedPiece.X;
		startTileY = SelectedPiece.Y;

		if(SelectedPiece.PieceType == PieceType.Normal){
//			myMove = new Move(startTileX, startTileY, tile.X, tile.Y);
			currentMove = new Move(startTileX, startTileY, tile.X, tile.Y, Player);
		} else {
//			myMove = new QueenMove(startTileX, startTileY, tile.X, tile.Y, Player);
			currentMove = new QueenMove(startTileX, startTileY, tile.X, tile.Y, Player);
		}

		UnRegisterMovableTilesForSelectedTileEvent(startTileX, startTileY);

//		currentMove.Moves.Add(myMove);

		Debug.Log("-------------------------------------");
		currentMove.IsCapturingMove(damaBoards[0]);
		if(currentMove.IsCapturingMove(damaBoards[0]) && SelectedPiece.GetCaptureMoves(tile.X, tile.Y).Count != 0){
			RegisterTilesForCaptureMove(tile.X, tile.Y);
			gameManager.ApplyMove(currentMove);
			currentMove = null;
		} else {
			gameManager.ApplyMove(currentMove);
			currentMove = null;
		}
	}

	private void RegisterSelectPieceEvent(){
		print ("RegisterSelectPieceEvent");

		foreach(IDamaBoard board in damaBoards){
			board.GetAllPiecesOfPlayer(this).ForEach(x => x.OnPieceSelection += PieceSelected);
		}
	}

	private void UnRegisterSelectedPieceEvent(){
		print ("UnregisterSelectedPieceEvent");

		foreach(IDamaBoard board in damaBoards){
			board.GetAllPiecesOfPlayer(this).ForEach(x => x.OnPieceSelection -= PieceSelected );
		}
	}

	private void RegisterMovableTilesForSelectedTileEvent(int startTileX, int startTileY){
		List<ITile> movableTiles = SelectedPiece.GetValidMoveTiles(startTileX, startTileY);

		movableTiles.ForEach( x => {
			x.OnTileSelection += TileSelected;
			x.HighlightTile(true);
		});

	}

	private void UnRegisterMovableTilesForSelectedTileEvent(int startTileX, int startTileY){
		List<ITile> movableTiles = SelectedPiece.GetValidMoveTiles(startTileX, startTileY);
		movableTiles.ForEach( x => {
			x.OnTileSelection -= TileSelected;
			x.HighlightTile(false);
		});
	}

	private void RegisterTilesForCaptureMove(int startTileX, int startTileY){
//		List<ITile> movableTiles = SelectedPiece.GetValidTileMoves(startTileX, startTileY);
//
//		movableTiles.ForEach( x => {
//			IMove move = new Move(startTileX, startTileY, x.X, x.Y);
//			if(move.IsCapturingMove(damaBoards[0])){
//				x.OnTileSelection += TileSelected;
//				x.HighlightTile(true);
//			}
//		});

		List<ITile> captureMoves = SelectedPiece.GetCaptureMoveTiles(startTileX, startTileY);

		captureMoves.ForEach( x =>{
			x.OnTileSelection += TileSelected;
			x.HighlightTile(true);
		});
	}
}
