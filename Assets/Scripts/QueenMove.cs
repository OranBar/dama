﻿using UnityEngine;
using System.Collections.Generic;

public class QueenMove : IMove {

	public int StartX{get; private set;}
	public int StartY{get;private set;}
	public int EndX{get;private set;}
	public int EndY{get;private set;}

//	public List<IMove> Moves {get;set;}

	public Player MovingPlayer{get;private set;}

	public QueenMove(){
		
	}

	public QueenMove (int startX, int startY, int endX, int endY, Player pieceOwner){
		this.StartX = startX;
		this.StartY = startY;
		this.EndX = endX;
		this.EndY = endY;
		this.MovingPlayer = pieceOwner;
//		Moves = new List<IMove>();
	}

	public QueenMove(IPiece piece, int EndX, int EndY, Player pieceOwner) : this (piece.X, piece.Y, EndX, EndY, pieceOwner){

	}

	public QueenMove(IPiece piece, ITile targetTile, Player pieceOwner) : this (piece.X, piece.Y, targetTile.X, targetTile.Y, pieceOwner){

	}

	public IPiece GetMovingPiece(IDamaBoard damaBoard){
		return damaBoard.GetPiece(StartX, StartY);
	}

	public ITile GetDestinationTile(IDamaBoard damaBoard){
		return damaBoard.GetTile(EndX, EndY);
	}

	public bool IsCapturingMove(IDamaBoard damaBoard){
		return GetCapturedPiece(damaBoard) != null;
	}

	public IPiece GetCapturedPiece(IDamaBoard damaBoard){
		Debug.Log(this.ToString());
		float moveDistance = Mathf.Abs(StartY - EndY);
		float x =  Mathf.Lerp( StartX, EndX, (1.0f/moveDistance)*(moveDistance-1) );
		float y =  Mathf.Lerp( StartY, EndY, (1.0f/moveDistance)*(moveDistance-1) );
		IPiece piece = damaBoard.GetPiece(Mathf.RoundToInt(x),Mathf.RoundToInt(y));
		if(piece != null && piece.Owner != MovingPlayer){
			return piece;
		} else {
			return null;
		}
	}

	public void ApplyMove (IDamaBoard damaBoard)
	{
		damaBoard.ApplyMoveCommand(this);
	}

	public override string ToString ()
	{
		return string.Format ("[Move: StartX={0}, StartY={1}, EndX={2}, EndY={3}]", StartX, StartY, EndX, EndY);
	}

}