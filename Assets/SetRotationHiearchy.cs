﻿using UnityEngine;
using System.Collections;

public class SetRotationHiearchy : OBMonoBehaviour {

	public Vector3 eulerRotation;

	protected override void EditorUpdate ()
	{
		base.EditorUpdate ();
		foreach(Transform child in this.transform){
			child.eulerAngles = eulerRotation; 
		}
	}

}
